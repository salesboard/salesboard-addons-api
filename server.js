var express = require('express'),
    app     = express(),
    server  = require('http').createServer(app)
;

server.listen(5000);

app.use(express.static( __dirname + '/' ));

console.log('Server is running under localhost:5000 ...');