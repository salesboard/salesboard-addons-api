// INIT

function consoleLog(text) {
    const myDate = new Date();
    const clock = `${myDate.getHours()}:${myDate.getMinutes()}:${myDate.getSeconds()}:${myDate.getMilliseconds()}`;
    const message = `[${clock}]` + ` ${text}`;
    document.getElementById('console').value += `${message}\n`;
    document.getElementById('console').scrollTop = document.getElementById('console').scrollHeight;
    console.log(message);
}

function onResponse() {
    return {
        onComplete(res) {
            if (res.message) {
                const str = JSON.stringify(res.message, undefined, 2);
                consoleLog(`onComplete: ${res.response} message: ${str}`);
            } else {
                consoleLog(`onComplete: ${res.response}`);
            }
        },
        onError(res) {
            consoleLog(`onError ${res.error}`);
            console.error(`onError: ${res.error}`);
        },
    };
}


document.addEventListener('DOMContentLoaded', () => {
    consoleLog('connectSalesboard');
    window.salesboard.connect({
            onOpen() {
                document.getElementById('connectionState').style.display = 'none';
                consoleLog('Connected!');
            },
        },
    );
});

function openAddon(addonName) {
    consoleLog(`openAddon ${addonName}`);
    window.salesboard.api.addon.open({name: addonName}, onResponse());
}

function switchAddon(addonName) {
    consoleLog(`switchAddon ${addonName}`);
    window.salesboard.api.addon.switchTo({name: addonName}, onResponse());
}

function closeAddon() {
    consoleLog('closeAddon');
    window.salesboard.api.addon.close({}, onResponse());
}

function getCurrentAddon() {
    consoleLog('getCurrentAddon');
    window.salesboard.api.addon.getCurrentAddon({}, onResponse());
}


function openPresentation(presentationName) {
    consoleLog(`openPresentation ${presentationName}`);
    window.salesboard.api.presentation.open({name: presentationName}, onResponse());
}

function openPitchcard(pitchcardName) {
    consoleLog(`openPitchcard ${pitchcardName}`);
    window.salesboard.api.pitchcard.open({name: pitchcardName}, onResponse());
}

function openForm(formName) {
    consoleLog(`openForm ${formName}`);
    window.salesboard.api.form.open({name: formName}, onResponse());
}


function generateDocument() {
    const pdfUrl = 'http://versions.salesboard.biz/addon-test/pdfTemplates/pdf2.pdf';
    consoleLog(`generateDocument from ${pdfUrl}`);

    const pdfMapping = {
        annotations: [
            {
                type: 'signature',
                page: 1,
                box: {
                    x: 300,
                    y: 550,
                    w: 80,
                    h: 60,
                },
                details: {
                    text: 'Handtekening Energieadviseur',
                    fontSize: 10,
                    fontName: 'Courier',
                    letterSpacing: 0,
                    color: '#000000',
                },
            },
            {
                type: 'signature',
                page: 1,
                box: {
                    x: 150,
                    y: 550,
                    w: 80,
                    h: 60,
                },
                details: {
                    text: 'Handtekening',
                    fontSize: 10,
                    fontName: 'Courier',
                    letterSpacing: 0,
                    color: '#000000',
                },
            },
        ],
    };

    const currentdate = new Date();
    const datetime = `${currentdate.getDate()}/${
    currentdate.getMonth() + 1}/${
        currentdate.getFullYear()} ${
        currentdate.getHours()}:${
        currentdate.getMinutes()}:${
        currentdate.getSeconds()}`;


    const docVariables = {
        customID: `sharewire ${datetime}`,
        summary: datetime,
        comments: 'Document Generated via Addon',
    };

    const str = JSON.stringify(docVariables, undefined, 2);
    consoleLog(`docVariables: ${str}`);

    window.salesboard.api.document.generateFromUrl({
        pdfUrl,
        pdfMapping,
        docVariables,
    }, onResponse());
}


function setSessionVar(key, value) {
    consoleLog(`setSessionVar ${key} : ${value}`);
    window.salesboard.api.session.setVariable({key, val: value}, onResponse());
}

function getSessionVar(key) {
    consoleLog(`getSessionVar ${key}`);
    window.salesboard.api.session.getVariable({key}, onResponse());
}

function getAllVariables() {
    consoleLog('getAllVariables');
    window.salesboard.api.session.getAllVariables({}, onResponse());
}

function clearVariables() {
    consoleLog('clearVariables');
    window.salesboard.api.session.clear({}, onResponse());
}

function setCurrentAudience() {
    consoleLog('setCurrentAudience');

    const audience = {
        hash: 'cff566c6837f53635d699257ada91acd',
        assign_to_user: null,
        assign_to_team: 1,
        lastUpdatedBy: 'Ricardo Cadete',
        lastChanged: '2018-11-07 13:32:41',
        fields: {
            building: '',
            city: 'Antwerpen',
            country: 'Belgium',
            door: '',
            flatNumber: '',
            floor: '',
            formattedAddress: '',
            houseName: '',
            houseNumber: 18,
            houseNumberCombined: '18',
            houseSuffix: '',
            households: '',
            levelNumber: '',
            name: '',
            numberFirst: '',
            numberLast: '',
            place: '',
            postcode: '2040',
            state: '',
            stateAbbreviation: '',
            street: 'Weeltjens',
            suite: '',
            unit: '',
        },
        coordinates: [
            4.3335603000000002538172338972799,
            51.37744159999999737920006737113,
        ],
        dataLayers: [],
        objType: 'lead',
        audienceTypes: [
            'lead',
        ],
    };

    window.salesboard.api.session.setCurrentAudience(audience, onResponse());
}

function getCurrentAudience() {
    consoleLog('getCurrentAudience');
    window.salesboard.api.session.getCurrentAudience({}, onResponse());
}

/**
 * @Deprecated
 */
function getCurrentLead() {
    consoleLog('getCurrentLead');
    window.salesboard.api.session.getCurrentLead({}, onResponse());
}

function getTimeline() {
    consoleLog('getTimeline');
    window.salesboard.api.session.getTimeline({}, onResponse());
}

function startSession() {
    consoleLog('startSession');
    window.salesboard.api.session.startSession({}, onResponse());
}

function stopSession() {
    consoleLog('stopSession');
    window.salesboard.api.session.stopSession({}, onResponse());
}

function openSessionOverlayer() {
    consoleLog('openSessionOverlayer');
    window.salesboard.api.session.openSessionOverlayer({}, onResponse());
}

function getSessionTime() {
    consoleLog('getSessionTime');
    window.salesboard.api.session.getSessionTime({}, onResponse());
}

function isSessionRunning() {
    consoleLog('isSessionRunning');
    window.salesboard.api.session.isSessionRunning({}, onResponse());
}

function onDocumentCreated() {
    consoleLog('onDocumentCreated');
    window.salesboard.api.session.onDocumentCreated({}, onResponse());
}

function alert(text) {
    consoleLog(`alert ${text}`);
    window.salesboard.api.system.alert({
        title: 'TEST',
        message: text,
        buttonTitle: 'Ok',
    }, onResponse());
}

function confirm(title, question, OKButtonTitle, CancelButtonTitle) {
    consoleLog(`confirm ${title} : ${question} : ${OKButtonTitle} : ${CancelButtonTitle}`);
    window.salesboard.api.system.confirm({
        title,
        question,
        OKButtonTitle,
        CancelButtonTitle,
    }, onResponse());
}

function leadSync() {
    consoleLog('requested leadSync()');
    window.salesboard.api.lead.sync({}, onResponse());
}

function getUser() {
    consoleLog('getUser');
    window.salesboard.api.system.getUser({}, onResponse());
}

function getUserSettings() {
    consoleLog('getUserSettings');
    window.salesboard.api.system.getUserSettings({}, onResponse());
}

function getDeviceLocation() {
    consoleLog('getDeviceLocation');
    window.salesboard.api.system.getDeviceLocation({}, onResponse());
}


function databaseSelection(tableName, columnNames, condition, size, from) {
    consoleLog(`databaseSelection table:${tableName} columns: ${columnNames} condition:${condition}`);
    columnNames = columnNames.replace(/\s+/g, '');
    const columnsArray = columnNames.split(',');
    window.salesboard.api.database.select({
        table: tableName,
        columns: columnsArray,
        where: condition,
        size,
        from,
    }, onResponse());
}


function statsLog(action, value) {
    consoleLog(`statsLog ${action} : ${value}`);
    window.salesboard.api.stats.log({action, value}, onResponse());
}


function connectorGetUserDetails() {
    consoleLog('connectorGetUserDetails');

    // Getting user ID first
    window.salesboard.api.system.getUser({}, {
        onComplete(res) {
            if (res.message && res.message.ID_User) {
                connectorGetUserDetailsWithId(res.message.ID_User);
            } else {
                consoleLog(`onComplete: ${res.response}`);
            }
        },
        onError(res) {
            consoleLog(`onError ${res.error}`);
            console.error(`getUser error: ${res.error}`);
        },
    });
}

function connectorGetUserDetailsWithId(userId) {
    const path = `user/${userId}`;
    const params = '';

    window.salesboard.api.connector.get({path, params}, onResponse());
}

function connectorRequestInternalTest(requestType) {
    consoleLog(`connectorRequestInternalTest ${requestType}`);

    // Getting first ID_User and ID_Team
    window.salesboard.api.system.getUser({}, {
        onComplete(res) {
            if (res.message) {
                const userID = res.message.ID_User;
                const teamID = parseInt(Object.keys(res.message.currentTeam)[0]);

                connectorRequestInternalTestParams(requestType, userID, teamID);
            } else {
                consoleLog(`onComplete: ${res.response}`);
            }
        },
        onError(res) {
            consoleLog(`onError ${res.error}`);
            console.error(`getUser error: ${res.error}`);
        },
    });
}

function connectorRequestInternalTestParams(requestType, userID, teamID) {
    const path = 'internal-test';
    const jsonParameters = {
        ID_User: userID,
        ID_Team: teamID,
        data: {
            obj1: 'A',
            obj2: 'B',
        },
    };

    switch (requestType) {
        case 'GET':
            window.salesboard.api.connector.get({path, params: jsonParameters}, onResponse());
            break;
        case 'POST':
            window.salesboard.api.connector.post({path, params: jsonParameters}, onResponse());
            break;
        case 'PUT':
            window.salesboard.api.connector.put({path, params: jsonParameters}, onResponse());
            break;
        case 'DELETE':
            window.salesboard.api.connector.delete({path, params: jsonParameters}, onResponse());
            break;
    }
}


function readSmartCard(smartCardType) {
    consoleLog(`readSmartCard(${smartCardType})`);

    const jsonParameters = {
        smartCardType,
    };

    // Reading from smart card
    window.salesboard.api.devices.readSmartCard({params: jsonParameters}, onResponse());
}

