var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/**
 * ---------------------------
 * Salesboard Javascript API
 * Internal release 2.9.2
 * 15 April 2020 - Sharewire
 * ---------------------------
 */

window.SBapiVersion = '2.9.2';
window.shouldconnect = false;
window.sent = {};
window.SalesBoardSocket;

function inIframe() {
  try {
    return window.self !== window.top;
  } catch (e) {
    return true;
  }
}

var salesboardAddonsApi = {
  /**
   * Connect is your main startup function.
   * You can optionally pass an options object that can contain:
   * onOpen(event) { console.log('connected!'),
   * onClose(event) { console.log('disconnected!');
   * onMessage(event) { console.log('message received!');
   */
  connect: function connect(options) {
    var ua = navigator.userAgent.toLowerCase();

    options.version = window.SBapiVersion;

    if (inIframe()) {
      // WebApp connection
      options.framework = 'webapp';
      window.SalesBoardSocket = new salesboardAddonsApi.SalesboardInterface(options);

      window.SalesBoardSocket.handleIframeAPIMessages = function (e) {
        if (e.data) {
          window.SalesBoardSocket.connection.onMessage(e.data);
        }
      };
      window.addEventListener('message', window.SalesBoardSocket.handleIframeAPIMessages);
    } else if (ua.indexOf('ipad') > -1 || navigator.vendor.toLowerCase().indexOf('apple') > -1 || window.useSockets) {
      // iOS
      options.framework = 'ios';
      window.SalesBoardSocket = window.iOSConnection = {};
      window.salesboard.connectOptions = options;

      if (window.webkit) {
        window.SalesBoardSocket = new salesboardAddonsApi.SalesboardInterface(options);
      }
    } else {
      // Android
      options.framework = 'android';
      window.SalesBoardSocket = new salesboardAddonsApi.SalesboardInterface(options);
    }

    console.log('Salesboard Javascript API using framework: \'' + options.framework + '\'');
    if (window.shouldconnect) window.SalesBoardSocket.connection.connect();
  },

  api: {
    /**
     * API functions dedicated to manipulating Salesboard addons
     */
    addon: {
      /**
       * open Opens an addon by ID (ID_Addon) or by name
       * string ID_Addon Id of the addon
       * string name Name of the addon
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      open: function open(parameters, events) {
        return window.SalesBoardSocket.send('addon.open', {
          parameters: parameters,
          events: events
        });
      },

      /**
       * close the addon.
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      close: function close(parameters, events) {
        return window.SalesBoardSocket.send('addon.close', {
          parameters: parameters,
          events: events
        });
      },

      /**
       * Close the currently active addon, and open a new one,
       * based on the 'ID_Addon' parameter passed to this function.
       * string ID_Addon of the addon.
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      switchTo: function switchTo(parameters, events) {
        return window.SalesBoardSocket.send('addon.switchto', {
          parameters: parameters,
          events: events
        });
      },

      /**
       * Returns basic information about the currently active addon.
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      getCurrentAddon: function getCurrentAddon(parameters, events) {
        return window.SalesBoardSocket.send('addon.getCurrentAddon', {
          parameters: parameters,
          events: events
        });
      }
    },
    /**
     * Api functions dedicated to manipulating window.sentations
     */
    presentation: {
      /**
       * open Opens a window.sentation by it's ID_window.sentation or by name
       * string ID_window.sentation Id of the window.sentation
       * string name Name of the window.sentation
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      open: function open(parameters, events) {
        return window.SalesBoardSocket.send('presentation.open', {
          parameters: parameters,
          events: events
        });
      }
    },
    /**
     * Api functions dedicated to manipulating form
     */
    form: {
      /**
       * open Opens a window.sentation by it's ID_Form or by name
       * string ID_Form Id of the form
       * string name Name of the form
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      open: function open(parameters, events) {
        return window.SalesBoardSocket.send('form.open', {
          parameters: parameters,
          events: events
        });
      }
    },
    /**
     * Api functions dedicated to manipulating pitch cards
     */
    pitchcard: {
      /**
       * open Opens a window.sentation by it's ID_Pitchcard or by name
       * string ID_Pitchcard Id of the pitchcard
       * string name Name of the pitchcard
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      open: function open(parameters, events) {
        return window.SalesBoardSocket.send('pitchcard.open', {
          parameters: parameters,
          events: events
        });
      }
    },

    /**
     * Api functions dedicated to manipulating document generation
     */
    document: {
      /**
       * Downloads a pdf template in the address especified at object 'pdfUrl',
       * in case 'pdfMapping' is defined applies the annotations to the pdf,
       * docletiables associates custom letiables to the documented generated
       * string pdfUrl The URL starting with http:// or https:// where a pdf will be located.
       * json pdfMapping A json object (JSON-encoded) with signatures mapping.
       * json docletiables A json object (JSON-encoded)
       * with key-values to be attached to the document.
       * You can use reserved 'customId' and 'summary'
       * to defined custom document id and custom title for this document;
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      generateFromUrl: function generateFromUrl(parameters, events) {
        return window.SalesBoardSocket.send('document.generate', {
          parameters: parameters,
          events: events
        });
      }
    },

    /**
     * Api functions dedicated to manipulating current session
     */
    session: {
      /**
       * Saves object 'val' under key 'key'  into the session.
       * Will last only for the current login. On app-terminate this has to be re-done.
       * string key The key to save this value to.
       * string val A sting or json object (JSON-encoded) object value to store under this key.
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      setVariable: function setVariable(parameters, events) {
        return window.SalesBoardSocket.send('session.setVariable', {
          parameters: parameters,
          events: events
        });
      },


      /**
       * Reads object 'key' from the session.
       * Will return NULL if key not available.
       * Session lets only last as long as the app is running.
       * string key The key to get the value for.
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      getVariable: function getVariable(parameters, events) {
        return window.SalesBoardSocket.send('session.getVariable', {
          parameters: parameters,
          events: events
        });
      },


      /**
       * Returns all letiables in current session.
       * Will return NULL if there is no session or empty if there is no letiables yet.
       * Session lets only last as long as the app is running.
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      getAllVariables: function getAllVariables(parameters, events) {
        return window.SalesBoardSocket.send('session.getAllVariables', {
          parameters: parameters,
          events: events
        });
      },


      /**
       * Delete all pairs key-value in session.
       * Will return NULL if key not available.
       * Session lets only last as long as the app is running.
       * string key The key to get the value for.
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      clear: function clear(parameters, events) {
        return window.SalesBoardSocket.send('session.clear', {
          parameters: parameters,
          events: events
        });
      },


      /**
       * Sets a Audience for the current session was started from.
       * Returns error if no current session active.
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      setCurrentAudience: function setCurrentAudience(parameters, events) {
        return window.SalesBoardSocket.send('session.setCurrentAudience', {
          parameters: parameters,
          events: events
        });
      },


      /**
       * Returns the Audience set for current session.
       * Returns error if no current session active or a session wasn't started from a lead.
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      getCurrentAudience: function getCurrentAudience(parameters, events) {
        return window.SalesBoardSocket.send('session.getCurrentAudience', {
          parameters: parameters,
          events: events
        });
      },


      /**
       * Returns a lead a current session was started from.
       * Returns error if no current session active or a session wasn't started from a lead.
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       *
       * @deprecated getCurrentAudience should be used instead, please note that the data format is different
       */
      getCurrentLead: function getCurrentLead(parameters, events) {
        return window.SalesBoardSocket.send('session.getCurrentLead', {
          parameters: parameters,
          events: events
        });
      },


      /**
       * Returns a current session timeline.
       * The timeline includes models opened during this session and their timestamps.
       * @event function onComplete a callback that is executed when the action has succeeded
       * @event function onError a callback that is executed when the action has failed
       */
      getTimeline: function getTimeline(parameters, events) {
        return window.SalesBoardSocket.send('session.getTimeline', {
          parameters: parameters,
          events: events
        });
      },


      /**
       * Starts the session, same as START SESSION button
       */
      startSession: function startSession(parameters, events) {
        return window.SalesBoardSocket.send('session.startSession', {
          parameters: parameters,
          events: events
        });
      },


      /**
       * Stops the session
       * This will trigger the next action defined at the Management Area
       * e.g.: 'open latest dashboard opened'
       */
      stopSession: function stopSession(parameters, events) {
        return window.SalesBoardSocket.send('session.stopSession', {
          parameters: parameters,
          events: events
        });
      },


      /**
       * Open the session Overlayer
       * Users may decide to jump to a different model or stop the session from this screen
       * e.g.: 'open latest dashboard opened'
       */
      openSessionOverlayer: function openSessionOverlayer(parameters, events) {
        return window.SalesBoardSocket.send('session.openSessionOverlayer', {
          parameters: parameters,
          events: events
        });
      },


      /**
       * Session time elapsed (in milliseconds) since the START SESSION
       */
      getSessionTime: function getSessionTime(parameters, events) {
        return window.SalesBoardSocket.send('session.getSessionTime', {
          parameters: parameters,
          events: events
        });
      },


      /**
       *  Checks if session is ongoing
       */
      isSessionRunning: function isSessionRunning(parameters, events) {
        return window.SalesBoardSocket.send('session.isSessionRunning', {
          parameters: parameters,
          events: events
        });
      },


      /**
       * To be called when the document creation has been finished successfully
       * This will trigger the next action defined at the Management Area
       * e.g.: 'go back to the pitchchard'
       */
      onDocumentCreated: function onDocumentCreated(parameters, events) {
        return window.SalesBoardSocket.send('session.onDocumentCreated', {
          parameters: parameters,
          events: events
        });
      }
    },
    /**
     * Custom database queries to tables published from salesboard servers.
     */
    database: {
      /**
       * Returns the result of custom query selection
       * of 1 or more collumns given a table and a optional condition
       * string table The table name where the query must run
       * against to (e.g. {'table': 'Products.csv'})
       * jsonArray columns A array of column names that must be selected
       * (e.g. {'columns': ['PRODUCT_NAME', 'PRODUCT_TYPE']})
       * string where Optional string that defines the condition
       * (e.g. {'where': 'PRODUCT_NAME' = 'RT_ZIP' AND 'DISTANCE' <= '100' })
       * string size Optional string that defines the size of page, default value is 1000
       * string from Optional string that defines what row the results should start from,
       * default value is 0
       */
      select: function select(parameters, events) {
        return window.SalesBoardSocket.send('database.select', {
          parameters: parameters,
          events: events
        });
      }
    },
    /**
     * Requests regarding leads
     */
    lead: {
      /**
       * Request tablet to start leads synchronization, does NOT synchronize regions, POI or streets
       */
      sync: function sync(parameters, events) {
        return window.SalesBoardSocket.send('lead.sync', {
          parameters: parameters,
          events: events
        });
      }
    },
    /**
     * Custom calls to log stats and measurables to the salesboard backend.
     */
    stats: {
      /**
       * Log a custom action named 'action' to the log table.
       * This will automatically be appended with timestamp,
       * geo coordinates and user id, cached until next
       * internet connection and window.sent to the backend where
       * a logreceiver could be invoked to handle this specific
       * log call.
       * string action The key to log this call under
       * (translates to backend extra logreceiverproperty)
       * int value The value to log. Can only be integer or long. It is optional.
       */
      log: function log(parameters, events) {
        return window.SalesBoardSocket.send('stats.log', {
          parameters: parameters,
          events: events
        });
      }
    },
    /**
     * API functions that affect user management.
     */
    system: {
      /**
       * Throw an alert with a title to the customer.
       * string title Title of the popup
       * string message Message to show
       * string buttonTitle The title of the button under the message, defaults to 'OK'.
       * @event function onComplete a callback that is executed when the user presses OK.
       * @event function onError a callback that is executed when the alert cannot be shown.
       */
      alert: function alert(parameters, events) {
        return window.SalesBoardSocket.send('system.alert', {
          parameters: parameters,
          events: events
        });
      },

      /**
       * Show an confirmation question with a title to the customer.
       * string title Title of the popup
       * string question Question to show
       * string OKButtonTitle The title of the OKbutton under the message, defaults to 'OK'.
       * string CancelButtonTitle
       * The title of the cancelbutton under the message, defaults to 'Cancel'.
       * @event function onComplete a callback that is executed when the user presses OK.
       * @event function onError a callback that is executed when the user presses Cancel.
       */
      confirm: function confirm(parameters, events) {
        return window.SalesBoardSocket.send('system.confirm', {
          parameters: parameters,
          events: events
        });
      },

      /**
       * grab currently loggedin user
       * @event function onError a callback that is executed when the action has failed
       */
      getUser: function getUser(parameters, events) {
        return window.SalesBoardSocket.send('system.getuser', {
          parameters: parameters,
          events: events
        });
      },

      /**
       * return the settings that the user defines at the Salesboard
       * @event function onError a callback that is executed when the action has failed
       */
      getUserSettings: function getUserSettings(parameters, events) {
        return window.SalesBoardSocket.send('system.getusersettings', {
          parameters: parameters,
          events: events
        });
      },


      /**
       * return the device location
       * @event function onError a callback that is executed when the action has failed
       */
      getDeviceLocation: function getDeviceLocation(parameters, events) {
        return window.SalesBoardSocket.send('system.getdevicelocation', {
          parameters: parameters,
          events: events
        });
      }
    },
    /**
     * API functions to connect to Salesboard servers
     */
    connector: {
      /**
       * GET request against Salesboard server, returns a JSONObject.
       * The authentication is handled automatically
       * @param string path
       * @param JSONObject params [optional]
       * @event function onComplete a callback that is executed when the user presses OK.
       * @event function onError a callback that is executed when the user presses Cancel.
       */
      get: function get(parameters, events) {
        return window.SalesBoardSocket.send('connector.get', {
          parameters: parameters,
          events: events
        });
      },

      /**
       * POST request against Salesboard server, returns a JSONObject.
       * The authentication is handled automatically
       * @param string path
       * @param JSONObject params [optional]
       * @event function onComplete a callback that is executed when the user presses OK.
       * @event function onError a callback that is executed when the user presses Cancel.
       */
      post: function post(parameters, events) {
        return window.SalesBoardSocket.send('connector.post', {
          parameters: parameters,
          events: events
        });
      },

      /**
       * PUT request against Salesboard server, returns a JSONObject.
       * The authentication is handled automatically
       * @param string path
       * @param JSONObject params [optional]
       * @event function onComplete a callback that is executed when the user presses OK.
       * @event function onError a callback that is executed when the user presses Cancel.
       */
      put: function put(parameters, events) {
        return window.SalesBoardSocket.send('connector.put', {
          parameters: parameters,
          events: events
        });
      },

      /**
       * DELETE request against Salesboard server, returns a JSONObject.
       * The authentication is handled automatically
       * @param string path
       * @param JSONObject params [optional]
       * @event function onComplete a callback that is executed when the user presses OK.
       * @event function onError a callback that is executed when the user presses Cancel.
       */
      delete: function _delete(parameters, events) {
        return window.SalesBoardSocket.send('connector.delete', {
          parameters: parameters,
          events: events
        });
      }
    },
    devices: {
      /**
       * Makes request to an external device, initially wrote to support smart readers using USB.
       * @param string path
       * @param JSONObject params [optional]
       * @event function onComplete a callback that is executed when the user presses OK.
       * @event function onError a callback that is executed when the user presses Cancel.
       */
      readSmartCard: function readSmartCard(parameters, events) {
        return window.SalesBoardSocket.send('devices.readSmartCard', {
          parameters: parameters,
          events: events
        });
      }
    }
  },

  /**
   * Private Salesboard interface,
   */
  SalesboardInterface: function SalesboardInterface(options) {
    var xsocket = {
      open: function open(e) {
        var logs = localStorage.getItem('errorQueue');

        this.connected = true;
        this.sysmsg('Connection established!');

        if (options.onOpen) {
          options.onOpen(e);
        }

        if (logs !== null) {
          try {
            logs = JSON.parse(logs);
          } catch (E) {
            console.log(E);
          }

          if (logs && logs.length > 0) {
            for (var i = 0; i < logs.length; i += 1) {
              console.error('addon.error' + logs[i]);
            }
          }

          localStorage.setItem('errorQueue', null);
        }
      },
      message: function message(event) {
        var result = void 0;

        if ((typeof event === 'undefined' ? 'undefined' : _typeof(event)) === 'object') {
          result = event;
        } else {
          try {
            result = JSON.parse(event.data ? event.data : event);
          } catch (E) {
            console.log(E);
          }
        }

        if (window.sent[result.UUID]) {
          window.sent[result.UUID].response = result.response;
          window.sent[result.UUID].responseReceived = true;

          if (!window.sent[result.UUID].events) {
            window.sent[result.UUID].events = {};
          }

          if (window.sent[result.UUID].events.onComplete && typeof window.sent[result.UUID].events.onComplete === 'function' && result.response === 'ok') {
            window.sent[result.UUID].events.onComplete(result);
          }

          if (result.response === 'nok') {
            if (window.sent[result.UUID].events.onError) {
              window.sent[result.UUID].events.onError(result);
            }
          }
          delete window.sent[result.UUID];
        }

        return result.response;
      },
      close: function close() {
        this.connected = false;
        window.SalesBoardSocket = window.NotConnectedSocket;
      },
      send: function send(func, params) {
        var thisID = Math.uuid();

        if (this.connection && this.connected) {
          window.sent[thisID] = {
            UUID: thisID,
            method: func,
            parameters: params.parameters,
            events: params.events,
            responseReceived: false,
            response: false
          };

          var message = {
            method: func,
            parameters: params.parameters,
            UUID: thisID
          };

          if (options.framework != 'ios') {
            message = 'api' + JSON.stringify(message);
          }

          this.connection.send(message);
        } else {
          this.connection.connect({
            onOpen: function onOpen() {
              this.send(func, params);
            }
          });
        }

        return thisID;
      },

      // send system msg to API server
      sysmsg: function sysmsg() {
        if (this.connection && this.connected) {
          var message = 'Connection done! v' + options.version;

          this.connection.send('system' + JSON.stringify({
            method: 'message',
            message: message,
            UUID: Math.uuid()
          }));
        }
      },

      connection: {
        connect: function connect() {
          xsocket.open();
        },
        disconnect: function disconnect(args) {
          xsocket.close(args);
        },
        send: function send(args) {
          try {
            if (options.framework === 'android') {
              window.APIAndroid.send(args);
            } else if (options.framework === 'ios' && window.webkit) {
              window.webkit.messageHandlers.salesboard.postMessage(args);
            } else if (options.framework === 'webapp') {
              window.parent.postMessage(args, '*');
            }
          } catch (e) {
            console.log(e);
          }
        },

        /**
         * android calls this directly on webview.loadurl;
         * from android: call
         * myWebview.loadUrl
         * ('javascript:window.SalesBoardSocket.connection.onMessage(encodedMessage)')
         * @param args
         */
        onMessage: function onMessage(args) {
          xsocket.message(args);
        }
      }
    };

    // XXX REQUIRED otherwise on events will not work!
    window.shouldconnect = true;

    return xsocket;
  }
};

Math.uuid = function () {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16;
    var v = c === 'x' ? r : r & 0x3 | 0x8;

    return v.toString(16);
  }).toUpperCase();
};

window.SalesBoardSocket = window.NotConnectedSocket = {
  send: function send() {
    window.salesboard.connect({});
  }
};

window.salesboard = salesboardAddonsApi;