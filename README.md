**Installing**

npm install


**BUILD**
(update only the file src/api.js, the build will deploy to the correct directories)
npm run build   










`npm install git+https://bitbucket.org/salesboard/salesboard-addons-api.git` 

This api is using ES2015 modules. You can import as follow:

`import salesboardAddonsApi from 'salesboard-addons-api';`

Or

`var salesboardAddonsApi = require("salesboard-addons-api");`

Otherwise, add the path in your index.html of your project. For example:

`<script src="node_modules/salesboard-addons-api/public/lib/api.js"></script>`

**Test addon**

Use the following path to open the test addon:

`node_modules/salesboard-addons-api/public/examples/addon-test/index.html`


